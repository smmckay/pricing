package com.citi.trading.pricing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Client component for the pricing service. Holds a publish/subscribe registry
 * so that many subscribers can be notified of pricing data on a given stock,
 * based on a single request to the remote service. When configured as a Spring bean,
 * this component will make HTTP requests on a 15-second timer using 
 * Spring scheduling (if enabled).
 * 
 * Requires a property that provides the URL of the remote service. 
 * 
 * @author Will Provost
 */
public class Pricing {

	public static final int MAX_PERIODS_WE_CAN_FETCH = 120;
	public static final int SECONDS_PER_PERIOD = 15;
	
	private Map<String, List<PriceWatcher>> priceWatchers = new HashMap<>(); 
	private Map<String, List<PricePoint>> priceWatcherStocks = new HashMap<>();
	
	public static PricePoint parsePricePoint(String CSV) {
		SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String[] fields = CSV.split(",");
		if (fields.length < 6) {
			throw new IllegalArgumentException
				("There must be at least 6 comma-separated fields: '" + CSV + "'");
		}
		
		try {
			Timestamp timestamp = new Timestamp(parser.parse(fields[0]).getTime());
			double open = Double.parseDouble(fields[1]);
			double high = Double.parseDouble(fields[2]);
			double low = Double.parseDouble(fields[3]);
			double close = Double.parseDouble(fields[4]);
			int volume = Integer.parseInt(fields[5]);
			
			return new PricePoint(timestamp, open, high, low, close, volume);
		} catch (Exception ex) {
			throw new RuntimeException("Couldn't parse timestamp.", ex);
		}
	}
	
	/**
	 * Requests data from the HTTP service and returns it to the caller.
	 */
	public void getPriceData() {
		
			Iterator it = priceWatchers.entrySet().iterator();
			String stock;
			while (it.hasNext()) {
				try {
					Map.Entry pair = (Map.Entry)it.next();
					stock = pair.getKey().toString();
					List<PriceWatcher> watchers = (List<PriceWatcher>) pair.getValue();
					
					
					it.remove();
					
					String requestURL = "http://will1.conygre.com:8081/prices/" + stock + "?periods=" + 1;
					List<PricePoint> prices = new ArrayList<PricePoint>();
					BufferedReader in = new BufferedReader(new InputStreamReader
							(new URL(requestURL).openStream()));
					in.readLine(); // header ... right? No way that could break ...
					String line = in.readLine();
					while (line != null) {
						PricePoint price = Pricing.parsePricePoint(line);
						price.setStock(stock);
						prices.add(price);
						line = in.readLine();
					}
					for (PriceWatcher watcher : watchers) {
						watcher.updatePricePoints(prices);
					}
					return;
					
				} catch (IOException ex) {
					throw new RuntimeException
							("Couldn't retrieve price for stock" + ".", ex);
				}
			}
		
	}
	/**
	 * Add a new watcher (listener)
	 * @param watcher watcher to add
	 */
	public synchronized void AddPriceWatcher(PriceWatcher watcher, String stock) {
		if (priceWatchers.containsKey(stock)) {
			priceWatchers.get(stock).add(watcher);
		} else {
			List<PriceWatcher> stockWatchers = new ArrayList<PriceWatcher>();
			stockWatchers.add(watcher);
			priceWatchers.put(stock, stockWatchers);
		}
	}
	
	/**
	 * Remove a given watcher (listener)
	 * @param watcher watcher to remove
	 */
	public synchronized void RemovePriceWatcher(PriceWatcher watcher, String stock) {
		priceWatchers.remove(watcher);
	}
	
	/**
	 * Quick test of the component.
	 */
	public static void main(String[] args) {
		Pricing pricing = new Pricing();
		Watcher watcher = new Watcher();
		Watcher watcher2 = new Watcher();
		Watcher watcher3 = new Watcher();
		pricing.AddPriceWatcher(watcher, "MRK");
		pricing.AddPriceWatcher(watcher2, "MRK");
		pricing.AddPriceWatcher(watcher3, "AAPL");
		pricing.getPriceData();
		
	}
	
	
	private static class Watcher
	implements PriceWatcher {

		@Override
		public void updatePricePoints(List<PricePoint> pricePoints) {
			System.out.println(pricePoints);
		}
		
	}
}


