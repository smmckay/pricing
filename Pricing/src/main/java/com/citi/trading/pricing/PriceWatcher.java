package com.citi.trading.pricing;

import java.util.List;

public interface PriceWatcher {

	public void updatePricePoints(List<PricePoint> pricePoints);
	
}
