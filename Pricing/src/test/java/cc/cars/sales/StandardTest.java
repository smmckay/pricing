/*
Copyright 2015 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.cars.sales;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import cc.cars.BadDataException;
import cc.cars.Car;

/**
Test for the {@link Standard Standard salesman}.

@author Will Provost
*/
public class StandardTest
{
    /**
    Factory method for a car to sell.
    */
    public static Car getCarOfMyDreams ()
        throws BadDataException
    {
        return new Car ("", "", 2000, "", "", 10000, 0, 0, Car.Handling.GOOD);
    }
    
    /**
    Helper method to assert that we can conduct a negotiation that leads
    to a deal.
    */
    public static void assertSaleSuccess (Standard salesman)
    {
        salesman.getAskingPrice ();
        assertEquals (8700, salesman.willSellAt (8000), .001);
        assertEquals (8490, salesman.willSellAt (8000), .001);
        assertEquals (8433, salesman.willSellAt (8300), .001);
        assertEquals (8433, salesman.willSellAt (8433), .001);
    }
    
    private Car carOfMyDreams;
    private Standard salesman;
    
    /**
    Initialize the target car and a standard salesman to sell it.
    */
    @Before
    public void setUp ()
        throws Exception
    {
        carOfMyDreams = getCarOfMyDreams ();
        salesman = new Standard (carOfMyDreams);
    }
    
    /**
    Check the asking price.
    */
    @Test
    public void testStandardDiscounts ()
        throws Exception
    {
        assertEquals (9000, salesman.getAskingPrice (), .001);
    }
    
    /**
    Test a successful negotiation.
    */
    @Test
    public void testSaleSuccess ()
        throws Exception
    {
        assertSaleSuccess (salesman);
    }
    
    /**
    Test a failed negotiation.
    */
    @Test
    public void testSaleFailure ()
        throws Exception
    {
        salesman.getAskingPrice ();
        assertEquals (8700, salesman.willSellAt (8000), .001);
        assertEquals (8490, salesman.willSellAt (8000), .001);
        assertEquals (8343, salesman.willSellAt (8000), .001);
        assertEquals (8343, salesman.willSellAt (8000), .001);
        assertEquals (8343, salesman.willSellAt (8000), .001);
    }
}
