/*
Copyright 2015 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package cc.cars.sales;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
Test suite for this package (and covers any sub-packages).
*/
@SuiteClasses
({ 
    HardNosedTest.class, 
    SimpleTest.class, 
    StandardTest.class
})
@RunWith(Suite.class)
public class TestSuite 
{
}
